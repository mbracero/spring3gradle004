## Apuntes

**Spring3 (boot, mvc, SECURITY + HIBERNATE + JUNIT) + gradle**

Generate java structure command:
`gradle init --type java-library`

Ejemplo simple de uso de spring security.

Authorization & Authentication (request) through Hibernate.

El formulario de login lo crea directamente spring.

Las URL's de login & logout son las siguientes:

> http://localhost:9000/login

> http://localhost:9000/logout



Probar directamente en navegador, existen 3 usuarios con tres roles definidos:

> user/password --> USER

> admin/password --> USER & ADMIN

> dba/password --> DBA & ADMIN



**Get currently authenticated user**

See:

> http://localhost:9000/api/items

> http://localhost:9000/api/items2


Las URL's a las que se pueden acceder son las siguientes:

> http://localhost:9000/api/items

> http://localhost:9000/admin/items

> http://localhost:9000/db/items


Dependiendo tanto del usuario con el que se hace login como de la URL a la que se accede tendremos distintos resultados (200, 401, 402, 404...).


**CURL**

user/password:

`curl -v --user user:password http://localhost:9000/api/items`


**Script DB (MySQL)**

```
CREATE DATABASE IF NOT EXISTS spring3gradle004;
USE spring3gradle004;

CREATE TABLE `spring3gradle004`.`user` (
`login` VARCHAR( 45 ) NOT NULL ,
`password` VARCHAR( 25 ) NOT NULL ,
`enabled` int( 1 ) NOT NULL ,
PRIMARY KEY ( `login` )
);

CREATE TABLE `spring3gradle004`.`user_role` (
`user_role_id` INT NOT NULL ,
`login` VARCHAR( 45 ) NOT NULL ,
`role` VARCHAR( 25 ) NOT NULL ,
PRIMARY KEY ( `user_role_id` )
);

INSERT INTO `spring3gradle004`.`user` (
`login` ,
`password` ,
`enabled`
)
VALUES (
'user', '1234', '1'
), (
'admin', '4321', '1'
), (
'dba', '1111', '1'
), (
'userdisabled', '2222', '0'
);


INSERT INTO `spring3gradle004`.`user_role` (
`user_role_id` ,
`login` ,
`role`
)
VALUES (
'1', 'user', 'ROLE_USER'
), (
'2', 'admin', 'ROLE_ADMIN'
), (
'3', 'dba', 'ROLE_ADMIN'
), (
'4', 'dba', 'ROLE_DBA'
), (
'5', 'userdisabled', 'ROLE_USER'
);
```
