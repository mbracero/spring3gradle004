package org.mbracero.user.dao;

import org.mbracero.user.model.User;

public interface UserDao {
	User findByUserName(String username);
}
