package org.mbracero.user.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.mbracero.user.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserDaoImpl implements UserDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public User findByUserName(String username) {
		List<User> users = new ArrayList<User>();
		 
		users = sessionFactory.getCurrentSession()
			.createQuery("from User where login=?")
			.setParameter(0, username)
			.list();
 
		if (users.size() > 0) {
			return users.get(0);
		} else {
			return null;
		}
	}

}
