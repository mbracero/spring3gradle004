package org.mbracero;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;


//This annotation tells Spring to auto-wire your application
@EnableAutoConfiguration
//This annotation tells Spring to look for controllers, etc.
@ComponentScan(value="org.mbracero.*")
//This annotation tells Spring that this class contains configuration
//information
//for the application.
@Configuration

@Import({ SecurityConfig.class, HibernateConfig.class })
/**
 * Require authentication to every URL in your application
 * Generate a login form for you
 * Allow the user with the Username user and the Password password to authenticate with form based authentication
 * Allow the user to logout
 * (...)
 * See: http://docs.spring.io/spring-security/site/docs/3.2.7.RELEASE/reference/htmlsingle/#hello-web-security-java-configuration
 */
public class Application {
	public static void main(String[] args) {
		// This call tells spring to launch the application and
		// use the configuration specified in LocalApplication to
		// configure the application's components.
		SpringApplication.run(Application.class, args);
	}
	
	@Bean
	public InternalResourceViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver =
			new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/WEB-INF/pages/");
		viewResolver.setSuffix(".jsp");
		return viewResolver;
	}
}
