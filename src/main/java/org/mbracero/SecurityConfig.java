package org.mbracero;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

//This annotation tells Spring that this class contains configuration
//information
//for the application.
@Configuration

@EnableWebMvcSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	@Qualifier("userDetailsService")
	UserDetailsService userDetailsService;
 
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService);//.passwordEncoder(passwordEncoder());
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		/**
		 * DEFAULT CONFIGURATION:
			    http
			        .authorizeRequests()
			            .anyRequest().authenticated()
			            .and()
			        .formLogin()
			            .and()
			        .httpBasic();
        **/
		
		/**
		 * MULTIPLE HTTP SECURITY:
		 * http://docs.spring.io/spring-security/site/docs/3.2.7.RELEASE/reference/htmlsingle/#multiple-httpsecurity
		 * We can configure multiple HttpSecurity instances just as we can have multiple <http> blocks. The key is to extend the WebSecurityConfigurationAdapter multiple times.
		 */
		
		http
		// There are multiple children to the http.authorizeRequests() method each matcher is considered in the order they were declared.
        .authorizeRequests()
        	// We specified multiple URL patterns that any user can access. Specifically, any user can access a request if the URL starts with "/resources/", equals "/signup", or equals "/about".
            .antMatchers("/resources/**", "/signup", "/about").permitAll()
            // Any URL that starts with "/admin/" will be resticted to users who have the role "ROLE_ADMIN". You will notice that since we are invoking the hasRole method we do not need to specify the "ROLE_" prefix.
            .antMatchers("/admin/**").hasRole("ADMIN")
            // Any URL that starts with "/db/" requires the user to have both "ROLE_ADMIN" and "ROLE_DBA"
            .antMatchers("/db/**").access("hasRole('ROLE_ADMIN') and hasRole('ROLE_DBA')")
            // Any URL that has not already been matched on only requires that the user be authenticated
            .anyRequest().authenticated()
            .and()
        .formLogin()
    		.and()
	    .httpBasic();
	}
	
	@Bean
	public PasswordEncoder passwordEncoder(){
		PasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder;
	}

}
