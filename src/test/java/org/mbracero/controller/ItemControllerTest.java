package org.mbracero.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mbracero.Application;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.ConfigFileApplicationContextInitializer;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration(classes = TestBean.class, initializers = ConfigFileApplicationContextInitializer.class)
@ContextConfiguration(classes = {Application.class},
	initializers = ConfigFileApplicationContextInitializer.class) // Retrieve application.properties
@WebAppConfiguration
public class ItemControllerTest {
 
    private MockMvc mockMvc;
    /*
    @Autowired
	@Qualifier("userDetailsService")
	UserDetailsService userDetailsService;
    
    protected UsernamePasswordAuthenticationToken getPrincipal(String username) {

        UserDetails user = this.userDetailsService.loadUserByUsername(username);

        UsernamePasswordAuthenticationToken authentication = 
                new UsernamePasswordAuthenticationToken(
                        user, 
                        user.getPassword(), 
                        user.getAuthorities());

        return authentication;
    }
    
    public static class MockSecurityContext implements SecurityContext {

        private static final long serialVersionUID = -1386535243513362694L;

        private Authentication authentication;

        public MockSecurityContext(Authentication authentication) {
            this.authentication = authentication;
        }

        @Override
        public Authentication getAuthentication() {
            return this.authentication;
        }

        @Override
        public void setAuthentication(Authentication authentication) {
            this.authentication = authentication;
        }
    }
    */
    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private AuthenticationManager am;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }
    
    public void clear() {
        SecurityContextHolder.clearContext();
    }
    
    /**
     * Since you want to test features implemented via Spring AOP, you need to use Spring TestContext framework to run tests against application context.
     * 
     * http://stackoverflow.com/a/5404030
     */
    protected void login(String name, String password) {
        Authentication auth = new UsernamePasswordAuthenticationToken(name, password);
        SecurityContextHolder.getContext().setAuthentication(am.authenticate(auth));
    }
    
    @Test
    public void userGetApiItems() throws Exception {
    	/*
    	UsernamePasswordAuthenticationToken principal = 
                this.getPrincipal("user");
    	
    	MockHttpSession session = new MockHttpSession();
        session.setAttribute(
                HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, 
                new MockSecurityContext(principal));
        */
        login("user", "1234");
        mockMvc
            .perform(
                    get("http://localhost:9000/api/items")
                    //.session(session) // Spring Security does not make the Authentication object available as a bean in the container, so there is no way to easily inject or autowire it out of the box.
            )
            .andExpect(status().isOk());
        clear();
    }
    
    @Test
    public void userGetApiItems2() throws Exception {
        login("user", "1234");
        mockMvc
            .perform(
                    get("http://localhost:9000/api/items2")
            )
            .andExpect(status().isOk());
        clear();
    }
    
    @Test
    public void dbaGetDbItems() throws Exception {
        login("dba", "1111");
        mockMvc
            .perform(
                    get("http://localhost:9000/db/items")
            )
            .andExpect(status().isOk());
        clear();
    }
    
    @Test
    public void adminGetDbItems() throws Exception {
        login("admin", "4321");
        mockMvc
            .perform(
                    get("http://localhost:9000/db/items")
            )
            .andExpect(status().isOk()); // TODO no funciona NO puede ser OK
        clear();
    }

    @Test
    public void badCredentials() {
        try {
        	login("user", "12345");
			mockMvc
			    .perform(
			            get("http://localhost:9000/api/items")
			    )
			    .andExpect(status().isOk());
		} catch (BadCredentialsException e) {
			Assert.assertTrue(true);
		} catch (Exception e) {
			Assert.assertTrue(false);
		} finally {
	        clear();
		}
    }

    @Test
    public void notFound() throws Exception {
    	login("user", "1234");
        mockMvc
            .perform(
                    get("http://localhost:9000/api/itemsZZZ")
            )
            .andExpect(status().isNotFound());
        clear();
    }
    
}
